package edu.stku.server.controllers.utils;

public enum RequestMethod {
    GET("GET"), POST("POST");
    private final String method;

    RequestMethod(String method) {
        this.method = method;
    }

    public String getMethod() {
        return method;
    }
}
