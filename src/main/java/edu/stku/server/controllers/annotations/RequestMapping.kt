package edu.stku.server.controllers.annotations

import edu.stku.server.controllers.utils.RequestMethod
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE, AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@Retention(value = RetentionPolicy.RUNTIME)
annotation class RequestMapping(val value: String, val method:

RequestMethod = RequestMethod.GET)
