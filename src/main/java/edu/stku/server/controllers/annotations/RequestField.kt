package edu.stku.server.controllers.annotations

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

@Target(AnnotationTarget.FIELD, AnnotationTarget.VALUE_PARAMETER)
@Retention(value = RetentionPolicy.RUNTIME)
annotation class RequestField(val value: String)
