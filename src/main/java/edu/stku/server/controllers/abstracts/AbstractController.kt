package edu.stku.server.controllers.abstracts

import edu.stku.server.config.ControllerRepository
import edu.stku.server.config.GlobalRepository
import edu.stku.server.controllers.annotations.RequestMapping

import javax.annotation.PostConstruct

abstract class AbstractController {
    fun getParams(name: String): Any? {
        return GlobalRepository.global[name]
    }
    val params: Map<String, Any>
        get() = GlobalRepository.repository.get()

    @PostConstruct
    fun init() {
        val declaredAnnotation = this.javaClass.getDeclaredAnnotation(RequestMapping::class.java)
        ControllerRepository.addController(declaredAnnotation.value, this.javaClass)
    }
}
