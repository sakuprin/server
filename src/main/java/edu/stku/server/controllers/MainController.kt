package edu.stku.server.controllers


import edu.stku.server.config.GlobalRepository
import edu.stku.server.controllers.abstracts.AbstractController
import edu.stku.server.controllers.annotations.RequestField
import edu.stku.server.controllers.annotations.RequestMapping
import edu.stku.server.controllers.utils.Body
import edu.stku.server.controllers.utils.HTML
import edu.stku.server.controllers.utils.RequestMethod
import edu.stku.server.controllers.utils.html
import org.springframework.stereotype.Controller
import java.util.*

@Controller
@RequestMapping(value = "/home", method = RequestMethod.GET)
class MainController : AbstractController() {
    @RequestMapping("/")
    fun index() = html {
        head {
            title { +"Server test title" }
        }
        body {
            menu()
            p {
                +" arguments :"
                ul {
                    for (arg in params)
                        li { +"${getText(arg.key)} = ${arg.value}" }
                }
            }
        }
    };


    @RequestMapping("/add")
    fun add(@RequestField(value = "name") name: String): HTML {
        if (GlobalRepository.global["names"] == null) {
            val get = GlobalRepository.global
            val arrayList = ArrayList<String>()
            get.put("names", arrayList)
        }
        val any = GlobalRepository.global["names"] as ArrayList<String>
        any.add(name)

        return html {
            head {
                title { +"Server test title" }
            }
            body {
                menu()
                p {
                    +"add argument ${name}"
                }
            }
        };
    }


    @RequestMapping("/info")
    fun info(): HTML {
        val any = if (GlobalRepository.global["names"] == null) Collections.EMPTY_LIST else GlobalRepository.global["names"] as List<*>
        return html {
            head {
                title { +"Server test title" }
            }
            body {
                menu()
                p {
                    ul {
                        for (value in any) {
                            li { +" ${value}" }
                        }
                    }
                }
            }
        };
    }
}

fun Body.menu() {
    val any = GlobalRepository.repository.get()["param->:user-agent"]
    div {
        h1 { +"Server test " }
        a(href = "http://localhost:8989/home") { +"index" }
        br { }
        a(href = "http://localhost:8989/home/add?name=$any") { +"add" }
        br { }
        a(href = "http://localhost:8989/home/info") { +"info" }
    }
}
