package edu.stku.server.http.readers


import java.io.BufferedReader
import java.io.IOException

interface InputHeadersReader {
    fun readerInputHeaders(bufferedReader: BufferedReader): Map<String, Any>
}
