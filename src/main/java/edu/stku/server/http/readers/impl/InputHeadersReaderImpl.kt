package edu.stku.server.http.readers.impl

import edu.stku.server.controllers.utils.RequestMethod
import edu.stku.server.http.readers.InputHeadersReader
import org.springframework.stereotype.Service
import java.io.BufferedReader
import java.util.*

@Service("inputHeadersReader")
class InputHeadersReaderImpl : InputHeadersReader {

    private var endIndex: Int = 0

    override fun readerInputHeaders(bufferedReader: BufferedReader): Map<String, Any> {
        val stringStringHashMap = HashMap<String, Any>()

        while (true) {
            val line = bufferedReader.readLine()
            if (line == null || line.trim { it <= ' ' }.length == 0) {
                break
            }
            for (requestMethod in RequestMethod.values()) {
                if (line.contains("HTTP/1.1")) {
                    if (line.contains(requestMethod.method)) {
                        parsHTTP11(line, requestMethod, stringStringHashMap)
                        continue
                    }
                }
            }
            if (line.contains(":")) {
                stringStringHashMap.put("param->:" + line.substring(0, line.indexOf(":")).trim { it <= ' ' }.toLowerCase(), line.substring(line.indexOf(":") + 1, line.length).trim { it <= ' ' })
            }
        }
        return stringStringHashMap
    }

    private fun parsHTTP11(line: String, requestMethod: RequestMethod, stringStringHashMap: HashMap<String, Any>) {
        stringStringHashMap.put("requestMethod", requestMethod.method.trim { it <= ' ' })
        endIndex = line.indexOf("?")
        if (endIndex == -1) {
            endIndex = line.indexOf("HTTP/1.1")
        }
        val startIndex = line.indexOf("/")
        var substring = line.substring(startIndex, endIndex)
        val split = substring.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        if (split.size == 2) {
            stringStringHashMap.put("controller", "/${ split[1].trim { it <= ' ' }}")
            stringStringHashMap.put("action", "/")
        } else {
            val controller = substring.substring(0, substring.lastIndexOf("/"))
            stringStringHashMap.put("controller", controller.trim { it <= ' ' })
            val action = split[split.size - 1]
            stringStringHashMap.put("action", "/${action.trim { it <= ' ' }}" )
        }

        val paramsIndex = line.indexOf("?")
        if (paramsIndex == -1) return
        substring = line.substring(paramsIndex + 1, line.indexOf("HTTP/1.1"))
        for (newLine in substring.split("&".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) {
            val param = newLine.split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            stringStringHashMap.put("param->:" + param[0].trim { it <= ' ' }, param[1].trim { it <= ' ' })
        }
    }
}
