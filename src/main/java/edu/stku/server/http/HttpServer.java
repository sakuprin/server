package edu.stku.server.http;


import edu.stku.server.http.readers.InputHeadersReader;
import edu.stku.server.http.writers.WriteResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;

@Service("httpServer")
public class HttpServer {
    @Value("${PORT}")
    private int port;
    private ExecutorService executorService;
    private InputHeadersReader inputHeadersReader;
    private WriteResponse writeResponse;

    @Autowired
    public void setInputHeadersReader(InputHeadersReader inputHeadersReader) {
        this.inputHeadersReader = inputHeadersReader;
    }

    @Autowired
    public void setWriteResponse(WriteResponse writeResponse) {
        this.writeResponse = writeResponse;
    }

    @Autowired
    public void setExecutorService(ExecutorService executorService) {
        this.executorService = executorService;
    }

    public void execute() {
        ServerSocket ss = null;
        try {
            ss = new ServerSocket(port);
            while (true) {
                Socket accept = ss.accept();
                executorService.execute(new SocketProcessor(accept, inputHeadersReader, writeResponse));
            }
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }

    }
}
