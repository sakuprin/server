package edu.stku.server.http


import edu.stku.server.config.ControllerRepository
import edu.stku.server.config.GlobalRepository
import edu.stku.server.http.readers.InputHeadersReader
import edu.stku.server.http.writers.WriteResponse

import java.io.*
import java.net.Socket

internal class SocketProcessor(private val socket: Socket, private val inputHeadersReader: InputHeadersReader, private val writeResponse: WriteResponse) : Runnable {
    private var inputStream: InputStream? = null
    private var outputStream: OutputStream? = null

    init {
        try {
            this.inputStream = socket.inputStream
        } catch (e: IOException) {
            println(e.message)
            throw RuntimeException(e)
        }

        try {
            this.outputStream = socket.outputStream
        } catch (e: IOException) {
            println(e.message)
            throw RuntimeException(e)
        }
    }


    override fun run() {
        try {
            val stringStringMap = inputHeadersReader.readerInputHeaders(BufferedReader(InputStreamReader(inputStream)))
            GlobalRepository.repository.set(stringStringMap)
            if (stringStringMap.values.contains("/favicon.ico")) {
                return
            }
            val html = getHTML(stringStringMap)
            val message = writeResponse.addHttpInfo(html)
            outputStream!!.write(message.toByteArray())
            outputStream!!.flush()
        } catch (throwable: Throwable) {
            println(throwable.message)
        } finally {
            try {
                socket.close()
            } catch (ignored: Throwable) {
                println(ignored.message)
            }

        }
        println("Client processing finished")
    }


    private fun getHTML(stringStringMap: Map<String, Any>): String {
        return ControllerRepository.execute(stringStringMap)
    }
}
