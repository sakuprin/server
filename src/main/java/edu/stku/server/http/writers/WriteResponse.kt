package edu.stku.server.http.writers


interface WriteResponse {
    fun addHttpInfo(s: String): String
}
