package edu.stku.server.http.writers.impl;

import edu.stku.server.http.writers.WriteResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class WriteResponseImpl implements WriteResponse {
    @Value("\r\n")
    private String SEPARATOR;
    @Value("${HTTP}")
    private String HTTP;
    @Value("${Server}")
    private String Server;
    @Value("${ContentType}")
    private String ContentType;
    @Value("${ContentLength}")
    private String ContentLength;
    @Value("${Connection}")
    private String Connection;

    public String addHttpInfo(String s) {
        StringBuilder res = new StringBuilder();
        res.append(HTTP).append(SEPARATOR);
        res.append(Server).append(SEPARATOR);
        res.append(ContentType).append(SEPARATOR);
        res.append(ContentLength);
        res.append(s.length()).append(SEPARATOR);
        res.append(Connection).append(SEPARATOR).append(SEPARATOR);
        res.append(s);
        return res.toString();

    }
}
