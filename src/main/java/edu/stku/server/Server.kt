package edu.stku.server


import edu.stku.server.http.HttpServer
import org.springframework.beans.factory.annotation.Required
import org.springframework.context.support.GenericXmlApplicationContext


class Server {
    private var httpServer: HttpServer? = null
    @Required
    fun setHttpServer(httpServer: HttpServer) {
        this.httpServer = httpServer
    }
   val start={httpServer!!.execute()}
}

fun main(args: Array<String>) {
    val genericXmlApplicationContext = GenericXmlApplicationContext("spring-config.xml")
    val server = genericXmlApplicationContext.getBean(Server::class.java)
    server.start()
}