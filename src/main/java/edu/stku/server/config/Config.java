package edu.stku.server.config;


import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

@Configuration
public class Config {
    @Value("10")
    private int threads;
    @Value("Подключения-%d")
    private String nameFormat;

    private ThreadFactory createThreadFactory() {
        return new ThreadFactoryBuilder().setNameFormat(nameFormat).setDaemon(true).build();
    }

    @Bean(name ="executorService" )
    public ExecutorService createExecutors() {
        return Executors.newFixedThreadPool(threads, createThreadFactory());
    }

}
