package edu.stku.server.config


import java.util.*
import java.util.function.Supplier

object GlobalRepository {
    public val repository = ThreadLocal.withInitial(Supplier<Map<String, Any>> { HashMap() })
    public val global = HashMap<String, Any>()
}
