package edu.stku.server.config

import edu.stku.server.controllers.abstracts.AbstractController
import edu.stku.server.controllers.annotations.RequestField
import edu.stku.server.controllers.annotations.RequestMapping
import edu.stku.server.controllers.utils.HTML
import java.lang.reflect.Method
import java.util.*

object ControllerRepository {
    private val controllerMap = HashMap<String, Class<out AbstractController>>()

    fun addController(action: String, controller: Class<out AbstractController>) {
        controllerMap.put(action, controller)
    }

    fun execute(params: Map<String, Any>): String {
        val controller = controllerMap[params["controller"] as String] ?: throw IllegalArgumentException("Ошибка конфигурации")
        val abstractAbstractController = controller.newInstance()
        return executeMethod(params, abstractAbstractController).toString()

    }

    private fun executeMethod(params: Map<String, Any>, controller: AbstractController): HTML {
        val paramsMethod = params["action"] ?: throw IllegalArgumentException("Ошибка конфигурации")
        try {
            val method = findMethod(controller, params, paramsMethod)
            val objects = createParamsContainer(method!!, params)
            return method.execute(controller, objects)
        } catch (e: Exception) {
            println(e.message)
            throw RuntimeException(e)
        }
    }

    fun findMethod(controller: AbstractController, params: Map<String, Any>, paramsMethod: Any) = controller.javaClass.methods.toList().
            filter { it.getDeclaredAnnotation(RequestMapping::class.java) != null }.
            filter { (paramsMethod as String).trim { it <= ' ' } == it.getDeclaredAnnotation(RequestMapping::class.java).value.trim { it <= ' ' } }.
            filter { it.getDeclaredAnnotation(RequestMapping::class.java).method.method == params["requestMethod"] }.
            firstOrNull()

    fun createParamsContainer(method: Method, params: Map<String, Any>): ArrayList<Any> {
        val objects = ArrayList<Any>()
        method.parameters.asList().
                filter { it.getDeclaredAnnotation(RequestField::class.java) != null }.
                map { it.getDeclaredAnnotation(RequestField::class.java) }.
                map { "param->:" + it.value.toLowerCase() }.
                forEach { objects.add(params[it]!!) }
        return objects
    }

}

fun Method.execute(controller: AbstractController, params: List<Any>) = this.invoke(controller, *params.toTypedArray()) as HTML
